  If you want to install any WOG extension that is packed to *.wog file,
put this *.wog file to this folder and run H3WUpd.exe file in WoG home folder.
  If installation runs fine, the *.wog file(s) will be renamed to *.wdn file(s).
If you have copies of *.wog files and need free space, you may delete *.wdn files
after the installation.
  If the installation goes with error, a corresponding log file will be placed to
WOGLOGS folder. If you cannot solve the problem yourself, please, zip all 
WoGDebug##.txt files in WOGLOGS folder and send to h3nl@narod.ru zipped with
a short description what was wrong.