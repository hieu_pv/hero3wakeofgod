  WoG PATCH 3.56

  To install WoG patch 3.56, you must to have WoG version 3.55 installed.

  WoG version: 356 
  ERM version: 250

  New Abilities:
1. New ERMable Treasure chests. They operate as a general Treasure
   chest until you control it with ERM.
2. New ERMable resource Mithril.
   It is controlled by ERM. By default any Mithril stack is a random
   resource (except wood or ore) but looks like Mithril. If you turn 
   Mithril "on" with ERM, you will have a new unique resource that is
   collected not only by human players but also by the AI.
3. Some new WoG objects (11).
4. Some new (mainly water) decorations (9).
5. After SG dies, SG Guards gain their speed back at the beginning
   of the next battle turn.
6. Check ERM356.TXT for lots of new ERM features.
7. New and updated Wogified scripts.
   Here's the full list of what script number has what:
   script00.erm - Wogify
   script01.erm - Map Rules
   script02.erm - Artifact Boost
   script03.erm - Skills Boost
   script04.erm - Arcane Tower
   script05.erm - Bank
   script06.erm - Demon Lord Summoning Artifact
   script07.erm - Fishing Well
   script08.erm - Junk Merchant
   script09.erm - Market Of Time
   script10.erm - Mushrooms
   script11.erm - Palace Of Dreams
   script12.erm - Living Skull
   script13.erm - Tavern Card Game
   script14.erm - Transformation Altar
   script15.erm - Mysterious Creature Dwelling
   script16.erm - Battle Academy
   script17.erm - Potions
   script18.erm - Alms House
   script19.erm - [reserved for Sash]
   script20.erm - Week Of The Monster
   script21.erm - Freelancer's Guild
   script22.erm - Monster Mutterings
   script23.erm - Ring Of Loyalty
   script24.erm - Enhanced Hint Texts
   script25.erm - Map Enhancements
   script26.erm - Artificer's
   script27.erm - Spellbook
   script28.erm - School Of Wizardry
   script29.erm - Chest
   script30.erm - Adventure Cave

  WOGIFY UPDATE
   The latest version adds the Freelancer's Guild, Ring Of Loyalty, 
   Artificer, Spellbook, School of Wizardry, and Adventure Cave objects 
   to maps, plus a new treasure chest.
  SKILLS UPDATE
   The latest version supports internet play (by not affecting human vs. 
   human network battles).
  CHEST
   This is a new script that adds a new kind of treasure chest.
  ADVENTURE CAVE
   This is a new script for an object where heroes can explore a small set 
   of caves, with a large variety of contents.
  MCD 1.3 (Mysterious Creature Dwelling)
   The Mysterious Creature Dwelling gives every player once every 10 days
   7 random creatures attached to visiting hero. 
   The Cost will be the same as recruiting in the Castle.
   Version 1.3 has changes in cost and can only be visited once per player 
   not per hero (like 1.2) for a better balance.
  ARTIFICER
   The Artificer upgrades once per day one Helmet, Sword, Shield or 
   Breastplate that a visiting Heroe wears (not in backpack). Visiting 
   Heroes will be asked which Artifact should get upgraded. He has to pay 
   the price and the Artifact changes to one Level higher.
  SPELLBOOK 
    Spellbooks found on the adventure map contains several spells. Also avoids 
    a problem that heroes could carry extra spellbooks in their backpacks.
  SCHOOL OF WIZARDRY
    Sells skills and spells, heroes may copy spells from scrolls to their 
    spellbook. Uses a different object than before.
  RING OF THE MAGI
   This script changes the Ring of the Magi combination artifact into a 
   standalone relic (cannot be built from combo parts nor split into them) 
   with a completely new power replacing the old one. The artifact's new 
   name is Ring of Loyalty. While this ring is equipped, an attacking hero
   will not abandon your cause after retreating from or being defeated 
   in battle, but will instead stay on the adventure map. In addition, 
   surviving troops (or the last few troops alive before the battle ends) 
   will loyally remain with the hero.
  MONSTER MUTTERINGS
   This script adds random text and dialogue to all monster stacks when 
   they're attacked. Most of it is quite silly and all of it is just for 
   fun. It works equally well with random monsters as it does with 
   specifically-placed monsters. If a monster has customized text, this 
   text will appear first, followed by the customized text after.
  ENHANCED HINT TEXT
   This script enhances the information you see when you right-click on 
   a dwelling. If you or an allied player own the dwelling, you'll be shown 
   how many creatures are still available to be hired there. If the dwelling 
   is unowned or owned by another non-allied player, you'll just be shown 
   the standard information.
  FREELANCER'S GUILD
   This script enhances all Freelancer's Guilds on a map, allowing a hero 
   who visits to hire a stack of monsters to hunt down an enemy. Either 
   a specific hero or an entire faction (colour) may be specified as 
   the target. If a specific hero is specified and the monsters successfully
   attack and kill that hero, they will then switch to hunting the faction 
   of that hero's colour. Wandering monsters move 1 space per turn and will 
   not move until two turns after they are initially hired. The cost for 
   hiring freelance monsters is 1/25th of normal recruiting cost. If 
   a monster cannot reach a hero or (hero or castle), it will move in that 
   direction and get as close as possible. Wandering monsters cannot travel 
   through subterranean gates, liths or across water.
  MAP ENHANCEMENTS
   This script gives the option of using or not using several other WoGify 
   scripts as well as the option of having Level 8 dwellings function like 
   standard dwellings instead. This script should be used in conjunction 
   with the following scripts: Week of Monsters, Enhanced Hint Text and
   Monster Mutterings.
  DEMON LORD SUMMONING ARTIFACT (Update)
   Version 1.1 fixes some bugs that allowed Asmodeus to be hired in 
   a tavern after retreating from or being used a combat. It also makes 
   minor message box changes and expands the right-click artifact 
   information to explain the new demon summoning power.
  WEEK OF MONSTERS (Update)
   Version 1.1 adds a check for the Diety of Fire - if it's present, it 
   will be "Week of the Imp" (with +15 Imp *and* +15 Familiar Growth).
   Version 1.2 adds special weeks for resources where mines of that type 
   produce double the number (e.g., 2 crystals for Week of the Sparkling 
   Crystals, 4 Wood for  Week of the Woodcutter).
  ALTAR OF TRANSFORMATION and LIVING SKULL updates fix some bugs but don't 
   change anything.
  BATTLE ACADEMY (Update)
    Uses a better way to check if it has been visited or not plus bug fixes.


  WoG Bug fixing:

1. Santa Gremlin (SG) now has Pixies instead of Air Elementals as guards
   if a hero is a Planeswalker or Elementalist.
2. Santa Gremlin (SG) now has Dwarves instead of Centaurus as guards
   if a hero is a Ranger or Druid. 
   The number of Dwarves is 1/2 (but>0) of SG number.
3. SG growth is set to 1 per player daily.
4. SG number attack multiplier is reduced from 128 to 64.
5. If you have a dwelling of level 8th creature, you can hire
   it since a first day in a corresponding town (if you have
   an advanced 7th level town dwelling built there).
6. The Darkness Dragon flies back now everytime to 
   the original position.
7. Multiplayer "Leave Army" bug fixed. When this is not your turn and 
   your hero stays next to other player hero, you will not be offered
   to leave other player's hero army.
8. Multiplayer "Demolish Town" bug fixed. When this is not your turn and 
   you cannot make any demolition in Towns.

  ERM Bug fixing:

1. MN:O, MN:R command produce no error if you use check or get syntax.


                              HMM3.5 Team (22.03.02)

 