CLS
@ECHO OFF
IF EXIST H3wog.exe ECHO SETTING UP WoG FOR THE ZONE
IF EXIST H3wog.exe pause
IF EXIST H3wog.exe COPY H3wog.exe H3wog.bak
IF EXIST H3wog.exe COPY Heroes3.exe Heroes3.bak
IF EXIST H3wog.exe DEL Heroes3.exe
IF EXIST H3wog.exe REN H3wog.exe Heroes3.exe
cls
IF NOT EXIST H3wog.exe ECHO READY TO PLAY WoG ON THE ZONE
PAUSE

