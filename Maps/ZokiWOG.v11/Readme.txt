Gods Rocks README.

This is my frst ERM-WOG map.
I make complete test of map, and everything is OK,
except some problems with ERM-WOG which I found during
map creation. Here is the summary:

1. Use version 3.52. T his version remove bug with
   Gods� Representatives.
2. Sometimes game crash, therefore save game frequently.
3. There is problem with changing the characteristics of
   Creatures. After some time they have old characteris-
   tics. I resolve this problem with event before creature
   dwelling which again and again change caracteristics of
   creature. It is not elegeant, but works in 95% of time.
4. There is also problem with some new caracteristics which
   are added to heroes, like new transformation of creatures.
   After some time hero 'forgot' this ability.

I put used scripts in ZIP.


Version 1.1

 In this version problems with Castle Town and rock at
 68/35/0 are correct.
 Thanks to Bruno and Alexey


Zoran Korveziroski
zkorvezir@yahoo.com
