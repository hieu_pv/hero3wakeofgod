ZVSE



** Object location: x=10, y=36, level=0
** Gods Snow Rock,  Place Castle Town on map

!#TR10/36/0:P1; [to make passable]
!#TR10/36/0:E0; [yellow trigger]
!#OB10/36/0:T81; [to turn into another object]
!#VRz1:S^Gods Snow Rock^; [right-click hint text]
!#OB10/36/0:H1;
!?OB10/36/0; [object trigger]
** Place Castle Town on map
!!IF:M^As you ride into a clearing, you notice a Gods Snow Rock, one of the legendary Gods rocks.^;
!!UN:I8/46/0/98/0; [Place Castle]
!!CA8/46/0:N^Paligrano^;
!!IF:M^You take this Gods Snow Rock and he create a Castle Town in your nearness.^;



** Object location: x=28, y=46, level=0
** Gods Sand Rock,  Transform Gold Dragons into Azure Dragons

!#TR28/46/0:P1; [to make passable]
!#TR28/46/0:E0; [yellow trigger]
!#OB28/46/0:T81; [to turn into another object]
!#VRz2:S^Gods Sand Rock^; [right-click hint text]
!#OB28/46/0:H2;
!?OB28/46/0; [object trigger]
** Transform Gold Dragons into Azure Dragons
!!IF:M^As you ride into a clearing, you notice a Gods Sand Rock, one of the legendary Gods rocks.^;
!!HE-1:X6/27/27/132;
!!IF:Q1/21/132/1^You take this Gods Sand Rock and he give you ability to transform Gold Dragons into Azure Dragons.^; [Display picture of new creatures]



** Object location: x=47, y=56, level=0
** Gods Lava Rock, +6 Atack

!#TR47/56/0:P1; [to make passable]
!#TR47/56/0:E0; [yellow trigger]
!#OB47/56/0:T81; [to turn into another object]
!#VRz3:S^Gods Lava Rock^; [right-click hint text]
!#OB47/56/0:H3;
!?OB47/56/0; [object trigger]
** Add +6 to the visiting heroe's Atack
!!IF:M^As you ride into a clearing, you notice a Gods Lava Rock, one of the legendary Gods rocks.^;
!!HE-1:F?v1/d0/d0/d0;
!!VRv1:+6;
!!HE-1:Fv1/d0/d0/d0;
!!IF:Q1/31/6/1^You take this Gods Lava Rock and he give you some advance skills.^; [Display picture of atack points bonus]



** Object location: x=59, y=15, level=0
** Gods Brown Rock, +6 Defense

!#TR59/15/0:P1; [to make passable]
!#TR59/15/0:E0; [yellow trigger]
!#OB59/15/0:T81; [to turn into another object]
!#VRz4:S^Gods Brown Rock^; [right-click hint text]
!#OB59/15/0:H4;
!?OB59/15/0; [object trigger]
** Add +6 to the visiting heroe's Defense
!!IF:M^As you ride into a clearing, you notice a Gods Brown Rock, one of the legendary Gods rocks.^;
!!HE-1:Fd0/?v2/d0/d0;
!!VRv2:+6;
!!HE-1:Fd0/v2/d0/d0;
!!IF:Q1/32/6/1^You take this Gods Brown Rock and he give you some advance skills.^; [Display picture of defense points bonus]



** Object location: x=30, y=24, level=0
** Gods Swamp Rock, +3 Power Skill & +3 Knowledge

!#TR30/24/0:P1; [to make passable]
!#TR30/24/0:E0; [yellow trigger]
!#OB30/24/0:T81; [to turn into another object]
!#VRz5:S^Gods Swamp Rock^; [right-click hint text]
!#OB30/24/0:H5;
!?OB30/24/0; [object trigger]
** Add +3 to the visiting heroe's Power Skill & Knowledge
!!IF:M^As you ride into a clearing, you notice a Gods Swamp Rock, one of the legendary Gods rocks.^;
!!HE-1:Fd0/d0/?v3/?v4;
!!VRv3:+3;
!!VRv4:+3;
!!HE-1:Fd0/d0/v3/v4;
!!IF:Q1/33/3/34/3/1^You take this Gods Swamp Rock and he give you some advance skills.^; [Display pictures of power skill & knowledge points bonus]



** Object location: x=69, y=59, level=0
** Gods Rough Rock, +50000 Experience

!#TR69/59/0:P1; [to make passable]
!#TR69/59/0:E0; [yellow trigger]
!#OB69/59/0:T81; [to turn into another object]
!#VRz6:S^Gods Rough Rock^; [right-click hint text]
!#OB69/59/0:H6;
!?OB69/59/0; [object trigger]
** Add +50000 to the visiting heroe's Experience
!!IF:M^As you ride into a clearing, you notice a Gods Rough Rock, one of the legendary Gods rocks.^;
!!HE-1:E?v5;
!!VRv5:+50000;
!!HE-1:Ev5;
!!IF:Q1/17/50000/1^You take this Gods Rough Rock and he give you some advance skills.^; [Display picture of experience points bonus]



** Object location: x=68, y=35, level=0
** Gods Dirt Rock,  Place a couple of Two Way Monoliths on map

!#TR68/35/0:P1; [to make passable]
!#TR68/35/0:E0; [yellow trigger]
!#OB68/35/0:T81; [to turn into another object]
!#VRz7:S^Gods Dirt Rock^; [right-click hint text]
!#OB68/35/0:H7;
!?OB68/35/0; [object trigger]
** Place  couple of Two Way Monoliths on map
!!IF:M^As you ride into a clearing, you notice a Gods Dirt Rock, one of the legendary Gods rocks.^;
!!UN:I56/70/1/45/3; [Place first Monolith]
!!UN:I1/38/1/45/3; [Place second Monolith]
!!IF:M^You take this Gods Dirt Rock and he create somewhere a  couple of Two Way Monoliths.^;

