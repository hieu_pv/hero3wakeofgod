The WoG Team
Heroes of Might and Magic III: In the Wake of Gods (WoG)
February 2003
Version 3.57f (final)
WARNING!
You must have WoG3.56 update installed prior to install this update.

Note: The information within this document is accurate as of the date 
of the release of this product.

------------------------- TABLE OF CONTENTS -------------------------------

1.   Installation
2.   ERM and WoG improvements in the single player WoG 3.57 (01.10.02)
3.   ERM and WoG improvements in the beta version 3.57m (18.11.02)
4.   ERM and WoG improvements in the final version of WoG 3.57f patch (26.02.03).
5.   ERM and WoG improvements in the final version of WoG 3.57f complete setup.
6.   Bug Fixing
7.   Bug Fixing in the beta version 3.57m (18.11.02)
8.   Known problems
9.   Contacting Us and Bug Reporting

----------------------------------------------------------------------------

1.   Installation

The WoG 3.56 must be installed prior to running this installation.

To install your copy of the WoG 3.57f onto your hard drive:

A) Uzip all files from the WoG 3.57f package (update357f.zip) to any folder.
  You will have there the next files:
    Install.exe
    unrar.dll
    lang.txt
    update357f.wog

B) Run Install.exe program and follow the instructions.

C) This patch changes some WoGification scripts, data files and executables.
   You can find all original versions of changed files in corresponding
   BACKUP folders. For example you will have such folders in:
   - Heroes Home folder;
   - DATA folder;
   - DATA\S folder.

----------------------------------------------------------------------------

2.   ERM and WoG improvements in the single player WoG 3.57 (01.10.02)

  1. COMMANDERS. Every hero now has a personal Commander who fights on the battlefield, 
gains experience, and goes up in level. You select which skills your Commander advances 
in (four out of six possible skills), and at higher levels you'll have the opportunity 
to select up to 6 (out of 15) special abilities that may become available depending on 
the skills you've chosen. Each Commander has a unique name and bio and each Commander 
type has its own unique ability and spell that it may cast.

  2. WOGIFY OPTIONS SCREEN. The brand new WoGify Options screen allows complete 
customization of WoGification to suit any player's preference. The button to reach this 
dialogue box is located in the Scenario Selection screen. In addition to choosing which 
new objects, global enhancement scripts and map rules will be in the game, the WoGify 
Options screen includes new options like "Towns start without forts" and "All 
cartographers are replaced", plus full disabling of movement spells and artifacts. WoG 
features such as Commanders, Town Demolition and Arrow Tower experience can also 
be enabled or disabled here.

  3. WOGIFICATION EXTENDED. The WoGify Options Master Settings gives you the choice 
between WoGifying only random maps, any WoG format map or even any Heroes III format map 
(RoE, AB, SoD or WoG).

  4. NEW COMBAT FEATURE. A new feature has been added to the combat screen to give your 
shooting creatures a choice each round between shooting or moving+attacking. Simply 
right-click on the Defend Button and you'll be given the choice. The same feature works 
for spellcasting creatures like faerie dragons and for attack+return creatures like harpies.

  5. NEW SCRIPTS. Many new scripts for WoGify have been added to make WoGified maps even 
more fun and diverse:

 - Mithril adds piles of Mithril to the map and gives you a way to spend it. 
Simply right-click on the Kingdom Overview button to see how much Mithril you have and 
to bring up a complete price list.

 - Mysticism Enhancement improves the Mysticism skill by making the spell points regained 
a percentage plus adding new exciting abilities at each skill level.

 - Sorcery Enhancement improves the Sorcery skill by adding the ability to right-click on 
treasures, artifacts and other objects to find out their value. Higher skill levels add 
additional abilities.

 - Summoning Stones is a new map object that lets you summon a stack of creatures from one 
of your towns into your army.

 - Karmic Battles (if enabled) adds additional monsters to the battlefield in most combats 
fought against neutral armies. The added monsters start out pretty weak but increase 
the more battles you fight.

 - Treasure Chest 2 adds a new type of treasure chest.

 - Cards of Prophecy enhances this tired old artifact with interesting new abilities.

 - Rebalancing Factions (if enabled) tweaks many heroes and monsters so as to provide 
better balance between the different town types.

 - Hero Specialization Boost (if enabled) adds extra abilities to heroes with spell and 
monster specialties.

 - Extended Battles (if enabled) tweaks monster stats so as to slow down the pace and 
increase the duration of most battles.

 - First Money (if enabled) gives an equal number of starting resources to all players, 
no matter what skill level you play at.

 - Living Scrolls turns most of the combat scrolls into Living Scrolls that have a chance 
of casting their own spell each round by themselves.

  6. SCRIPT UPDATING. Many older scripts have been updated to make them even better 
than before. Bugs have been fixed and scripts have been revamped to work with the WoGify 
Options and the new Commander artifacts. In a few cases, completely new features have been 
added.

  7. COMMANDER ARTIFACTS. Ten unique Commander artifacts are included in the map editor 
and may show up in WoGified maps. Many of the Commander artifacts will continually increase 
the bonus they grant the Commander after several battles have been won. These added 
incremental bonuses are lost if the Commander dies or unequips the artifact.

  8. NEW OBJECTS. Nine new objects have been added to the game. These are: a new treasure 
chest, a barrel (if not scripted it acts like a treasure chest), two new signs, a new magic 
well, a magic book on a pedestal (new shrine), and three "empty" scriptable objects.

  9. SCRIPT EDITOR. The script editor has been updated to make it even friendlier than ever 
with comprehensive built-in help and new features.

  10. ERM EXTENSIONS. More than a dozen new ERM features have been added for your scripting 
pleasure, including: AI movement control, mouse click triggers for the town screen, 
hero screens, and combat screen, full Commander support via ERM, post-event and post-object 
triggers, artifact equipping/unequipping triggers, global monster and skill name replacement, 
and more! Please read ERM357.TXT for full details on the new ERM features.

----------------------------------------------------------------------------

3.   ERM and WoG improvements in the beta version 3.57m (18.11.02)

  1. UN:G2 command. Lets you set the text and picture for hero specialities.
  !!UN:G2/#1/#2/#3;
   #1-number of hero.
   #2-type of settings:
       2=description text
       3=picture (see below)
   #3-z variable index (0 to restore)

  You can also set a picture for specialty.
  !!UN:G2/#1/3/#2;
   #1-number of hero.
   #2-number of the picture (0-155)

The pictures match the current hero specialty pictures so for example, if 
you wanted a picture of gems, you could use 111 (Saurug's specialty) for
the picture number. In the future, additional specialty pictures will be 
added.

  2. !?IP# trigger - multiplayer support.
  Now if one player's hero attacks another player's hero, there
will be the next sequence:
  a) !?BA0 trigger for "A"(attacker) player switches;
  b) !?IP0; trigger switches for "A" player
  c) data transferred to "D"(defender) player:
     - "A" player's Hero info;
     - "A" player's Hero Commander info (completely);
     - v vars set v9001-v10000;
     - all potentially changed monster statistics;
  d) all sent data received at "D" player side;
  e) !?IP1; trigger switches for "D" player;
  f) !?BA0; trigger switches for "D" player;
  Now the battle takes place.
  If "A" player wins, no WoG data transferred.
  if "D" player wins, there will be the next sequence:
  a) "A" player goes to wait until "D" player gains levels;
  b) "D" player's Hero and Commander gain levels;
  c) !?IP2; trigger switches for "D" player;
  d) data transferred tp "A" player:
     - "D" player's Hero sec. skills (all 28), level and exp.;
     - "D" player's Hero Commander info (completely);
     - v vars set v9001-v10000;
  e) !?BA1; trigger switches for "D" player;
  f) "A" player goes from waiting until "D" player gains levels;
  g) all sent data received at "A" player side;
  h) !?IP3; trigger switches for "A" player;
  i) !?BA1; trigger switches for "A" player;

  As you can see, monsters, heroes and Commanders statistics should be
identical now. As a side effect I can say that 10 sec. skills for Hero
is a multiplayer safe rule. All 28 sec. skills transfered in both
directions if needed.

----------------------------------------------------------------------------

4.   ERM and WoG improvements in the final version of WoG 3.57f patch (26.02.03).

  1. New ERM Receiver: VC - control of ERM variable usage (Variable Logging).
  This receiver may be used in instruction (!#VC:...) syntax otherwise it 
does not make sense.
  Commands:
  C - clear the list of used ERM variables
  B - start logging used ERM variables
  E - stop logging used ERM variables.
  W - write log to ERMVarsUsed.LOG file.
  Y - start a cross-reference checking section
  N - stop a cross-reference checking section
  Comments.
  You can use B and E command more than one time to control different parts 
of script. The effect will be summed up.
  Example:
  !#VC:CB;
  !!VRv10:...;
  !#VC:E;
  !!VRv11:...;
  !#VC:B;
  !!VRv12:...;
  !#VC:EWC;
  You can also use more than one Y and N sections.
  Example:
  !#VC:CB;
  !!VRv10:...;
  !!VRv11:...;
  !#VC:Y;
  !!VRv11:...;
  !!VRv12:...;
  !#VC:N;
  !!VRv12:...;
  !!VRv14:...;
  !#VC:Y;
  !!VRv14:...;
  !#VC:N;
  !!VRv13:...;
  !#VC:EWC;
  The next vars will be signed as cross referenced: v11 and v14;
  and not cross-referenced: v10,v12,v13
  (v12 is not cross-referenced because it was not used before checking)
  You have logged now:
    flags : 1...1000
    single letter vars : f...t
    v vars: v1...v10000
    w vars: w1...w100
    z vars: z1...z1000
    All marked with:
      - 'p' if used as a parameter;
      - '&' if used in & condition section;
      - '|' if used in | condition section;
      - '*' if cross-referenced;
    Timers: TM1...TM100 
      - marked with 't' if a trigger was found
      - marked with 'r' if a receiver or instruction was found
    Functions/Loops: FU1...FU30000, DO1...DO30000
      - marked with 't' if a trigger (function definition) was found
      - marked with 'r' if a receiver or instruction (function call) was 
found
      - marked with 'd' if a receiver or instruction (cycle call) was found
  It cannot log an indirect referenced vars like this:
    !!VRvx10:...
  The index of v var is unknown here at loading time for x10 is undefined 
then.
  You can log vars from several script files. Say, you can put !#VC:CB; in 
script00.erm file and !#VC:EWC; to script40.erm and you will get the 
complete list of vars in all script files from 0 to 40.
  The log file is created at map loading and called as "ERMVarsUsed.log". 
If such file exists, it will be completely overwritten.
  We propose to initialise all vars that you may use in your script like 
this:
  !#VR...:S0;
  or so to be sure that this var will be logged.
  We recommend to use Y and N section to check your own script for 
vars/timers/functions that are used in your script and in other scripts.
  For this you should set a number for your script file to higher than 
any other scripts, log vars in all scripts and use Y and N section in your 
script file.
  Example:
   You have scripts number 0,1,2,3,4,5...50 and want to check your script 
for cross-reference with them.
   1. Number your script as script99.erm (condition: 99 > 50).
   2. Add "start logging" in the beginning of the first script file 
(script00.erm):
     !#VC:CB;
   3. Add "start cross-reference checking" in the beginning of your script 
file (script99.erm):
     !#VC:Y;
   4. Add "stop cross-reference checking" in the end of your script file 
(script99.erm):
     !#VC:N;
   5. Add "stop logging and write" in the end of your script file 
(script99.erm):
     !#VC:EWC;
  Now, if your script uses variables/triggers/functions that were used in 
the scripts 0...50, all of them will be marked in the log file with '*'.
  If you want to make the same for your internal map scripts, assume 
scripts number as a day of Timed Events and do the same.

  2. Extended !!BA syntax. New command is added 'A'.
    A$ - get or check (NOT set) an AI only battle
    $ = 1 if this battle is theoretical only (complete AI battle)
    $ = 0 if this is a real battle or quick battle.
  So you can always check whether you can use battle receivers for this 
battle or not (see comments below).
  Comments.
  You may use set syntax, but this means nothing.
  If we have any battle on the map (Hero to Hero, Hero to Monsters, Hero 
to defended object...), the !?BA# trigger works always. Also a corresponding 
receiver !!BA... works fine. 
  But. If this is a battle WITHOUT human (completely AI battle, not quick 
auto battle!), there is NO REAL battle simulation. AI just calculates 
possible losses using AI value of creatures, no more.
  This means that in such battles none of the next receivers may be used 
(corresponding triggers also will not work in this case):
  !!BG, !!BM, !!BH, !!BU, !!BF
  This is important for all of them treat a combat manager as filled by 
correct values, though actually, this is not.
  Game will most probably crash if this is the first battle since game 
start or a result is undetermined otherwise.
  Warning! Even for Human battle, the combat manager IS NOT READY in !?BA0 
trigger section. So you cannot use
!?BA0;
!!BH0:N?v426;
  BH receiver should be used only in BH trigger section.
  I see that using BA:A may be not the easiest way to track it and made 
another change.
  We have flag1000 as a marker of HUMAN/ai owner of a hero.
  For the battle trigger !?BA# this flag is set to inverse "complete AI 
battle" flag. So you can use it to check like this:
!?BA1&1000;
  Here you run !?BA trigger section only if this is a real battle (there 
is at least one human as an opponent).
  In many senses this flag1000 value is the same as before
  I think that this is enough for other battle triggers: !?BG, !?BR and 
!?BF must not be called in theoretical battles (complete AI battles).
  Now a usage of !!BG, !!BM, !!BH, !!BU, !!BF in complete AI battle is 
controlled in the ERM interpreter and you will get the corresponding error 
message.

  3. New Trigger !?CO - Commander
   !?CO0; is called before opening the Commander Dialog
   !?CO1; is called after closing the Commander Dialog
   !?CO2; is called after buying a Commander in a Town
   !?CO3; is called after reviving a Commander in a Town
   Flag 1000 set to 1 only if the owner of the hero is a human player who 
sits before the screen.
   Flag 999 as usual shows whether this turn is a human player who sits 
before the screen.
   You can use !!HE-1:... to get the current hero info and !!CO-1:... to 
get current Commander info.
   Vars v998,v999,v1000 as usual shows the position of the current hero 
on the map.

  4. New variable types - floating point: e1...e100
   They are used the same way as y vars:
   - not stored in the saved game;
   - are local in every function;
   - filled with 0 at every function/cycle start;
   - restored when return from other function call.
   You may use them to set parameters but the main feature is to use it to 
calculate floating point expression and then store it to integer variable.
   You can use +,-,* and : commands of VR receiver for e vars in floating 
point calculations though all numbers should be integer in the expression.
   You can also put e vars in the message with %E# command. In this case 
only 3 digits after dot is shown.
   Here is the example:
!#VRe5:S32;  !#IF:M^e5 is "%E5"^; [ e5=32.0 ]
!#VRe5:*5;  !#IF:M^e5 is "%E5"^;  [ e5=160.0 ]
!#VRe5::200;  !#IF:M^e5 is "%E5"^; [ e5=0.8 ]
!#VRe5:+e5*10+5:10;  !#IF:M^e5 is "%E5"^; [ e5=2.1 ]
!#VRv100:Se5; !#IF:M^v100 is "%V100"^; [ v100=2 ]

  5. New hardcoded flag to make all external dwelling accumulate creatures 
every week.
  You can turn it on !#UN:P7/1; or off !#UN:P7/0; (default).

  6. New hardcoded flag to make all external dwelling guards accumulate 
creatures every week. 
  You can turn it on !#UN:P8/1; or off !#UN:P8/0; (default).

  7. Extended syntax for !!PO receiver.
  !!PO...:V#/$;
  !!PO...:B#/$;
  First V#/$ - set/check/get an integer value (-32768...32767).
  You can use up to 4 values.
  # - index of the value to set (0...3)
  $ - value to set/check/get
  Second B#/$ - set/check/get a big integer value (-2147483648...2147483647).
  You can use up to 2 values.
  # - index of the value to set (0...1)
  $ - value to set/check/get
 Comments.
  All integer values in the ERM are big so if you store it with a command 
like this !!PO...:V1/y4; the value of y4 will be truncated in the internal 
PO storage (y4 itself stay unchangeable) you should be sure that you have 
a correct value before set. Command B has no such nuance.

  8. Script localization support.
  I was asked to simplify a way to localize scripts.
  The point is that if an author change something in his script, it takes 
much time to check all script and change all massages to other language.
  I was proposed to make a separate file for every script (optional) that 
will keep all texts from the script.
  Now you can make a file Script##.ert that has a specific format:
Index(integer) [tab delimiter] Text [tab delimiter] Any Comments ...
  Two columns are obligatory, others - optional (Comments are not loaded 
and saved to the game).
  One line - one Item.
Index means an index that will be used in the script to access the text.
  Index may be from 0 to 1000000.
  If Index is 0 (or empty), the line is ignored and will not be loaded.
  If Index is between 1 and 1000, the text will be automatically copied to 
corresponding z variables at script loading (before executing any 
instruction in the script).
  So if you have the next line:
123[tab]This is a Text[tab]Any Comments
  and in your script there is the next instruction:
!#IF:M1/z123;
  you will see the message at load time "This is a Text"
  This is the way to move your z vars initialisations out of the
script to the file.
  It was:
!#VRz321:S^Hello, stranger!^;
...
  Now you delete this line of the script and add a line to the ert file:
321[tab]Hello, stranger![tab]Start up message when a Hero enter to...
  To get access to the text you should use the same way as for z vars but 
the number is higher than 1000.
  If you have in your script a z var that is used for storing a constant 
name or text, you can free this z var and does not use it at all if you add 
a line to the ert file with higher than 1000 number and change your code.
  Then, if you have something like this:
!#IF:M^You are not ready yet.^;
  you should change your script to something like this:
!#IF:M1/z123456;
  and add a line to the ert file:
123456[tab]You are not ready yet.[tab]Any Comments
  You cannot use this constant extra z vars for changing any default text 
in the game (artifact name and so). This is the same limitation that is 
used for local z vars (z-1...z-10). With one exception. You can use the 
static extra z vars as a hint text in HT:T,P,Z,V, OB:H, HO:H. The only 
restriction is that the index should be not higher than 32000 in this case.
  You may compare constant extra z vars in & and | sections.
  You can use a formatted string also:
 3333[tab]Hello, %Z55555![tab]...
55555[tab]Orin[tab]...
  You can include static extra z vars as %Z format item.
  As for loading. ert file is loaded only if there is a corresponding erm 
file and only if it is loaded correctly.
  All extra static z vars are common for every ert file. So if you use 
the same number in one or different ert files, you will have the 
corresponding message and the map will not be loaded.
  I recommend to divide the space of extra z vars the next vay:
Index 1000...10000 reserved yet
Index 10000...20000 (100 vars per script for hint purposes).
  Script00 10000...10099,
  Script01 10100...10199...
  (index is 1##00 ... 1##99, where ## is a script number)
Index 20000...100000 reserved yet.
Index 100000...200000 (1000 vars per script for non-hint purposes).
  Script00 100000...100999,
  Script01 101000...101999,
  (index is 1##000 ... 1##999, where ## is a script number)

  9. I extended a syntax for OW:H. Now you may use three arguments:
H#1/#2/#3;
  As before:
  #1 is an owner index (0...7,-1 as currently active)
  #2 is an index of v variable
  Additional:
  #3 is an index of a hero the owner has.
     = 0 - store number of all heroes the owner has in v[#2]
     > 0 - index, store the number of a hero the owner has to v[#2]
           if there is no hero with index, the v[#2] is not changed
  There is NO check or get syntax!
  Example:
!!OW:H1/99/0; store a number of heroes the Blue player has to v99
!!OW:H1/100/1; store the number of the first hero the Blue player has to v100
!!OW:H1/100/2; store the number of the second hero the Blue player has to v100
!!OW:H1/100/3; store the number of the third hero the Blue player has to v100
!!OW:H1/100/4; store the number of the fourth hero the Blue player has to v100

  10. !!BA:H will return now -2 instead of -1 if there is no hero.

  11. You may protect your map now with SD.EXE utility.
  Just run it:
SD.EXE mymap.h3m
  The map mymap.h3m will be protected.
  This means that you cannot open this map in the editor anymore but may 
be played.
  WARNING! No backup copy of the map will be created and there is no way 
to remove the protection. So do not forget to make a backup copy of the 
map before protecting it.

  12. New !!FU:E; command allows you now to exit the current function 
immediately.

----------------------------------------------------------------------------
5.   ERM and WoG improvements in the final version of WoG 3.57f complete setup.

  1. First Aid message bug fixed. If troops were revived by the AI, the human 
player see a message about it.

  2. Karmic Battles obstacle bug fixed. Karmic creatures will no longer be placed 
on obstacles.

  3. Karmic Battles sometimes triggered in MP player vs. player battles 
(with strange results). This should no longer occur.

  4. Living Scrolls Anti-magic Garrisons bug fixed. Living Scrolls were not always 
disabled correctly in anti-magic garrisons. Now they will be.

  5. Alms House "cheat" corrected. It was possible to use an Alms House and 
Arcane Tower combination to get infinite free stat points for your hero. Now each 
hero may only visit an Alms House once per game. Visiting a different Alms House 
won't get around the restriction (they're a chain).

  6. Second Campaign "Gaia" first map hero transfer bug fixed.

----------------------------------------------------------------------------

6.   Bug Fixing

  1. Commander Info Screen hang up bug is fixed. The game hung if you try 
to open the Commander Info Screen (click on the handshake icon in the Hero 
Info Screen). This also happened if a Hero gets experience and his/her 
Commander gains a level.

  2. Bug fixed. Customer script may be added with script##.ers file to 
the WoGifiing Options Dialog.

  3. SoD bug fixed.
  It happened if the AI calculates if he/she can hire the neutral creature 
in an external dwelling and has the Angelic Alliance (AA).
  It seems that at least one AI hero may have AA later in the game and it 
seems to be a common reason for game crashes on the 4th month or later.
  And the bigger map you have, the more chance to get this bug.

  4. Commander Death Stare bug fixed.
  The maximum number of killed with Death Stare creatures was 1.
  Now it is Commander_Level/Monster_Level as stated in the description.

  5. Commander reviving price is corrected.
  Gold=(Level*Level+Level%2)*50
  So it gives:
  Level  1 = 100
  Level  2 = 200
  Level  3 = 500
  Level  4 = 800
  Level  5 = 1300
  ...
  Level 20 = 20000

  6. MP transfer time is dramatically reduced (by several times!).
  Now the first turn transfer is as large as usual but all later transfers 
are much less.
  For making a saved game smaller you can use now the Script Editor feature 
that is called "anti comment". This will remove all non-functional parts of 
any script making it up to two times smaller.

  7. MP Bug fixed. If an AI Hero attacks a Human at another PC (AI players 
run at the PC with the latest Human player), the Commander info was not 
transferred properly.

  8. Now all monsters stacks (wandering or stationary) may be checked for 
"unexpected legion" (number is equal to 4095 that is the maximum value) 
that may happen in the game. If you try to check such a stack using a right 
mouse click on it, you will get an offer to remove this stack from the map.
  You can delete visible and invisible stacks this way (until invisible 
Wandering Monster problem is fixed completely).

  9. The system of Commander Ballistas and First Aid Tents is changed.
  If the hero-owner has a corresponding stack, the number of Commander's 
units is added to this stack. If there is no Hero's stack, the new stack 
is placed at the same place.
  The summoned Commander's monsters are also located now at the new place 
(before catapult).

 10. You cannot Dismiss or Rename Commander and move Commander's Artifacts 
to/from a Commander when it's not your turn.
  WARNING! If you open a Commander dialog when it's not your turn, it will 
block the MP game until you close it.
  For example, if an enemy hero attacks your hero/town/garrison, a battle 
screen will not appear until you close the Commander dialog.
  So, here is a hint. Either do not check your Commander info at your turn 
or do it quickly to avoid MP connection interrupt.

 11. Bug fixed. Invisible Legion of Wandering Monsters will not appear on 
the Adventure Map anymore.

 12. The campaign "A Life of A-d-v-e-n-t-u-r-e" was updated by the author.

 13. SoD MP bug fixed. 
In MP battle if AI hero attacked a distant human hero and a defending (human) 
hero won and got any new secondary skills, they were not shown correctly 
when this player got his/her turn back and game crashes if you tried to see 
a description of such incorrectly shown skills.

----------------------------------------------------------------------------

7.   Bug Fixing in the beta version 3.57m (18.11.02).

  1. HE:X command bug fixed. If you had a lot of HE:X commands, the game 
may not be loaded later.

  2. Conflux Commander banished monsters number calculation fixed.

  3. IF:Q works with Mercury correctly now. Before there was a problem with 
two pictures and a yes/no type question--if one picture was mercury it 
wouldn't set the flag.

  4. SoD AI behaviour on adventure map is restored.

  5. New features for WoG map may be controlled now with instructions 
(!#UN:P). Now if a map has internal scripts and does not have !#UN:P3/# 
command (Commander enabling/disabling), the map is running in WoG 3.56 
environment (without Commanders and all). But if the map has this command 
(no matter !#UN:P3/0 or !#UN:P3/1), the map is treated as a new one and 
runs in WoG 3.57 environment (so all hard coded WoG features are taken 
from WoGification setup or installed by !#UN:P instructions).
  This will fix the problem with the impossibility to enable Commanders 
with !#UN:P3/0 instructions unless used in the timer trigger section.

  6. "Monsters Power" artifact now acts correctly in MP mode.

  7. Crash bug fixed. If AI attacks a Hero with Commander only, game crashed.

  8. Another bug fixed. If one hero takes an enemy town and an enemy hero 
with Commander only is in a town garrison, there was no battle. Now should 
work properly.

  9. If Hero dies at the Placed Event location, program might crash 
(WoG 3.57 only bug). That happened in post visit trigger processing. 
Now fixed. Post visit trigger for killed hero is not called.

 10. If you load a map with internal scripts and being WoGified, you lose 
an ability to have a Commander. Now fixed if you save and load a game in 
the WoG 3.57m or 3.57f version.

----------------------------------------------------------------------------

8.   Known Problems

Current Script Bugs (May 20, 2003)

1. Mysticism Script crash bug. Using the Mysticism skill to check the stats of 
a hero carrying certain artifact combinations will sometimes cause a crash.

2. Adventure Cave and Living Skull AI "stuck" bugs. An AI hero would sometimes 
get stuck later in the game, spending all its time entering and exiting one of 
these objects. New code has now been added to attempt to fix this but we're not 
sure if it's working or not. Please let us know.

3. First Aid Script plus Astral Spirit (Conflux Commander) bug. With this 
combination, the script may try to revive creatures stolen by the Astral Spirit 
following the battle.

4. If you enable renaming of towns, sometimes after renaming the "rename town box" 
will again appear when clicking certain places in the town. However, this will 
only happen once (until you rename again).

Current WoG bugs
  
1. Artifact Merchant often changes artifacts daily instead of monthly. (I haven't 
specifically seen it lately but I haven't heard of it being fixed either so it's a 
"maybe" unless someone can confirm one way or the other.)

2. Several Commanders, when run by the AI, will cast their spells on War Machines 
if present, rather than troops.

3. The Paladin, when run by the AI, will cast Cure on stacks that don't need curing.

4. In MP battle if an enemy Hero is a Planeswalker or Elementalist and has a Commander, 
you will get a bit wrong message that some of your creature is vanished. The number 
that you will see will be less than the real one.

5. If you lost a game and start a new MP one you may get warning that you cannot save 
a game at your turn a load time. The corresponding message will appear for all other 
human players. You may just ignore these messages.

6. Any time that you try to put a new WoG creature into the Necro's Skeleton Transformer 
the game crashes.

----------------------------------------------------------------------------

9.   Contacting Us and Bug Reporting

  If you find a bug, please ZIP:
- the saved game (if you think it is needed);
- the TXT file with the description of the bug and how we can reproduce 
it on the saved game and/or a new map;
- the version of WoG you started the game with;

  In the message Subject or Body please write "WoG 3.57f testing" and 
any other info we may need in your opinion and/or other files you want 
to send us. 
  Please, do not send any other files, except the zip file; we will ask 
you for them later if needed.

  Then send this message to one of the addresses below:
    h3nl@narod.ru
    WoG@StrategyPlanet.com
    zvs@mail.ru

  Thank you for your cooperation. Let us make WoG better together.

----------------------------------------------------------------------------

22 May 2003
WoG Team 

--The End--
